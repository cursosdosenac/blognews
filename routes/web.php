<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UsuarioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
});

Route::get('/noticias', function () {
    return view('noticias.index');
});

Route::get('/noticias/visualizar', function () {
    return view('noticias.visualizar');
});


Route::get('/buscar', function () {
    return view('site.buscar');
});

Route::get('/noticias/categoria', function () {
    return view('home.index');
});

Route::get('/login', function () {
    return view('login.login');
});

Route::get('/admin', function () {
    return view('admin.home');
});

Route::prefix('/admin')->group(function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.home');

    //Usuários
    Route::get('/usuarios', [UsuarioController::class, 'index'])->name('admin.usuarios.index');
    Route::get('/usuarios/cadastrar', [UsuarioController::class, 'create'])->name('admin.usuarios.cadastrar');
    Route::post('/usuarios/cadastrar', [UsuarioController::class, 'store'])->name('admin.usuarios.cadastrar');
    Route::get('/usuarios/editar/{id}', [UsuarioController::class, 'edit'])->name('admin.usuarios.editar');
    Route::put('/usuarios/editar/{id}', [UsuarioController::class, 'update'])->name('admin.usuarios.editar');
    Route::delete('/usuarios/deletar/{id}', [UsuarioController::class, 'destroy'])->name('admin.usuarios.deletar');
});
