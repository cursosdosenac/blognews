<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{

    public function index()
    {
        $usuarios = User::all();
        return view('admin.usuarios.index', [
            'usuarios' => $usuarios
        ]);
    }


    public function create()
    {
        return view('admin.usuarios.cadastrar');
    }


    public function store(Request $request)
    {
        //dd($request); // forma de depurar o código

        //Validação dos Dados
        $request->validate([
            'nome' => 'required',
            'email' => 'required|email|unique:usuarios',
            'password' => 'required|min:8|confirmed',
        ]);


        //Inserindo os dados na tabela usuarios
        try {
            $usuario = new User();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->role =  $request->role;
            $usuario->password = bcrypt($request->password);
            $usuario->save();

            //Caso tudo de certo, o usuário será redireciondado para a view index com uma mensagem
            return redirect()->route('admin.usuarios.index')->with('sucesso', 'Usuário cadastrado com sucesso!');
        } catch (\Exception $e) {
            //Caso tudo aconteça algum erro no momento do cadastro
            //dd($e->getMessage());
            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente!');
        }
    }


    public function show($id)
    {
        return view('admin.usuarios.visualizar');
    }


    public function edit($id)
    {
        return view('admin.usuarios.editar');
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
